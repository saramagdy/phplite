<?php

return [
    'host' => '127.0.0.1',
    'port' => '3306',
    'database' => 'phplite',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'collation' => 'utf8_general_ci',
];