<?php 

/**
 * PhpLite Framework
 * @Author Sara Magdy  saramagdyahmed6@gmail.com 
 */

 /**
  * Register the autoloader
  * |---------------------------
  * load the autoloader that will generate classes
  */
  require __DIR__.'/../vendor/autoload.php';


  /**
  * Bootstrap the application
  * |---------------------------
  * Do action from the framework
  */
  require __DIR__.'/../bootstrap/app.php';


  /**
   * Run the application
   * Handle the request and send response
   */
    Application::run();